<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

    public static function getCustomLogo()
    {
        if ( get_custom_logo() ) {
            $custom_logo_id = get_theme_mod( 'custom_logo' );
            $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
            $image = aq_resize( $image[0], 54, 54, true );
            $home = get_bloginfo( 'url' );
            $img = "<a href='{$home}' class='custom-logo-link' rel='home'>";
            $img .= "<img src='{$image}' alt='Cloud1500 Logo'>";
            $img .= "</a>";
            //return get_custom_logo();
            return $img;
        }
    }

    public static function getOptions(  ) {
        return get_fields( 'options' );
    }

    public function headerImage()
    {
        if ( is_home() ) {
            $pageID = get_option( 'page_for_posts' );
        } elseif ( is_single() ) {
            $pageID = get_the_ID();
            if ( ! has_post_thumbnail( $pageID ) ) {
                $image = get_fields( 'options' )[ 'images' ][ 'replacement_image' ][ 'url' ];
                return $image;
            }
        } else {
            return;
        }

        $thumbURL = get_the_post_thumbnail_url( $pageID, 'full' );
        $thumb = aq_resize( $thumbURL, 1920, 400, true, true, true );

        return $thumb;
    }
}
