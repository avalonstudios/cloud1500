import AOS from 'aos/dist/aos';
export default {
  init() {
    // JavaScript to be fired on all pages
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
    $('a[href*="#"]')
      // Remove links that don't actually link to anything
      .not('[href="#"]')
      .not('[href="#0"]')
      .click(function(event) {
        // On-page links
        if (
          location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
          &&
          location.hostname == this.hostname
        ) {
          // Figure out element to scroll to
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          // Does a scroll target exist?
          if (target.length) {
            // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            $('html, body').animate({
              scrollTop: target.offset().top,
            }, 1000, function() {
              // Callback after animation
              // Must change focus!
              var $target = $(target);
              $target.focus();
              if ($target.is(':focus')) { // Checking if the target was focused
                return false;
              } else {
                $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                $target.focus(); // Set focus again
              }
          });
        }
      }
    });

    $( '.hamburger' ).on( 'click', (  ) => {
      //let $navBar = $( '.navbar' );
      let $mainNavigation = $( '.main-navigation' );

      animationClick( '.main-navigation li', 'fadeInLeft', 'fadeOutLeft', 750 );
      animationClick( '.navbar .nav-underlay-bottom', 'fadeInUp', 'fadeOutDown', 250 );
      animationClick( '.navbar .nav-underlay-left', 'fadeInRight', 'fadeOutRight', 500 );

      if ( ! $mainNavigation.hasClass( 'open' ) ) {
        $( '.navbar' ).addClass( 'open' );
        $( '.main-navigation' ).addClass( 'open' );
      } else {
        $( '.navbar' ).removeClass( 'open' );
      }
    } );

    function animationClick( el, animationIn, animationOut, duration ) {
      let element = $( el );
      if ( ! element.hasClass( 'animated-out' ) ) {
        element.addClass( 'animated-in animated ' + animationIn ).css( 'animation-duration', duration + 'ms' );
        animationEnd( element, 'in', animationIn, animationOut );
      } else {
        element.addClass( 'animated ' + animationOut ).css( 'animation-duration', duration + 'ms' );
        animationEnd( element, 'out', animationIn, animationOut );
      }
    }

    function animationEnd( el, animInOut, animationIn, animationOut ) {
      let element = $( el );
      //wait for animation to finish before removing classes
      element.one( 'animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd', () => {
        if ( animInOut == 'in' ) {
          setTimeout( (  ) => {
            element.removeClass( 'animated-in animated ' + animationIn ).css( 'animation-duration', '' ).addClass( 'animated-out' );
          }, 500 );
        } else if ( animInOut == 'out' ) {
          $( '.main-navigation' ).removeClass( 'open' );
          setTimeout( (  ) => {
            element.removeClass( 'animated-out animated ' + animationOut ).css( 'animation-duration', '' );
          }, 500 );
        }
      });
    }

    $( '.main-navigation li.menu-item-has-children .fas' ).click( function( e ) {
      e.preventDefault();

      var $this = $( this );

      if ( $this.next().hasClass( 'show' ) ) {
        $this.next().removeClass( 'show' );
        $this.next().slideUp( 350 );
      } else {
        $this.parent().parent().find( '.sub-menu' ).removeClass( 'show' );
        $this.parent().parent().find( '.sub-menu' ).slideUp( 350 );
        $this.next().toggleClass( 'show' );
        $this.next().slideToggle( 350) ;
      }
    });

    $( window ).on( 'scroll', onScroll );
    $( window ).on( 'load', stickMenu );

    var $banner  = $( '.banner' );

    function onScroll(  ) {
        stickMenu(  );
    }

    function stickMenu(  ) {
      let $scrollTop   = $( window ).scrollTop();

      if ( $scrollTop > 0 ) {
        $banner.addClass( 'scrolled' );
      } else {
        $banner.removeClass( 'scrolled' );
      }
    }

    $( window ).on( 'click', function() {
      //let root = document.documentElement;
      //root.style.setProperty( '--c_pink', 'green' )
    } );

    setTimeout( (  ) => {
      doHeroSlider(  );
      doArticlesSlider(  );
      doThemesDemo();
      removePreloader();

      AOS.refresh(  );

    }, 500 );

    AOS.init({
      mirror: true,
      duration: 700,
    });
  },
};

function removePreloader() {
  $( '.preload-wrapper' ).fadeOut();
}

function doHeroSlider(  ) {
  var settings = {
    'autoplay'      : true,
    'dots'          : false,
    'autoplaySpeed' : 3000,
    'fade'          : true,
    'speed'         : 1500,
    'lazyLoad'      : 'ondemand',
  };

  let $slides = $( '.hero-image-slider' );

  $slides.on( 'beforeChange', ( e, slick, currentSlide ) => {
    $( slick.$slides[ currentSlide ] ).find( '.hero-content' ).addClass( 'fadeOut' ).removeClass( 'fadeIn' );
    $( slick.$slides[ currentSlide ] ).find( '.hero-title' ).addClass( 'fadeOutRight' ).removeClass( 'fadeInLeft' );
    $( slick.$slides[ currentSlide ] ).find( '.hero-text' ).addClass( 'fadeOutLeft' ).removeClass( 'fadeInRight' );
    $( slick.$slides[ currentSlide ] ).find( '.btn-wrap' ).addClass( 'fadeOutDown' ).removeClass( 'fadeInUp' );
  } );

  $slides.on( 'afterChange', ( e, slick, currentSlide ) => {
    $( slick.$slides[ currentSlide ] ).find( '.hero-content' ).removeClass( 'fadeOut' ).addClass( 'fadeIn' );
    $( slick.$slides[ currentSlide ] ).find( '.hero-title' ).removeClass( 'fadeOutRight' ).addClass( 'fadeInLeft' );
    $( slick.$slides[ currentSlide ] ).find( '.hero-text' ).removeClass( 'fadeOutLeft' ).addClass( 'fadeInRight' );
    $( slick.$slides[ currentSlide ] ).find( '.btn-wrap' ).removeClass( 'fadeOutDown' ).addClass( 'fadeInUp' );
  } );

  $slides.slick( settings );
}

function doArticlesSlider(  ) {
  var settings = {
    'autoplay'      : true,
    'dots'          : false,
    'autoplaySpeed' : 3000,
    'fade'          : true,
    'speed'         : 1500,
    'lazyLoad'      : 'ondemand',
  };

  let $slides = $( '.articles-slider' );

  $slides.slick( settings );
}

function doThemesDemo() {
  var settings = {
    'autoplay'        : true,
    'dots'            : true,
    'arrows'          : false,
    'autoplaySpeed'   : 3000,
    'speed'           : 1500,
    'slidesToShow'    : 3,
    'slidesToScroll'  : 3,
    'lazyLoad'        : 'ondemand',
  };

  let $slides = $( '.themes-demo' );

  $slides.slick( settings );
}
