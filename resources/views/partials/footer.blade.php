<footer class="content-info">
  <div class="container">
    <div class="footer-navigation" id="footerMenuNav">
      @if ( ! is_front_page() )
      {!! wp_nav_menu([
        'theme_location' => 'primary_navigation',
        'container_class' => 'footer-menu',
        'menu_class' => 'navbar-nav mr-auto',
        'depth' => 1,
        ])
      !!}
      @else
      {!! wp_nav_menu([
        'theme_location' => 'front_navigation',
        'container_class' => 'footer-menu',
        'menu_class' => 'navbar-nav mr-auto',
        'depth' => 1,
        ])
      !!}
      @endif

      @include ( 'partials.menus.social-menu', [ 'place' => 'social-footer' ] )
    </div>
    @php //dynamic_sidebar('sidebar-footer')
    @endphp
    <div class="copyright-design">
      <div class="copyright">Copyright &copy; {{ get_bloginfo( 'name' ) }} {{ date( 'Y' ) }}</div>
      <div class="sep">|</div>
      <div class="designer">Designed by <a href="https://avalonstudios.eu" target="_blank" rel="nofollow">Avalon Studios</a></div>
      <div class="sep">|</div>
      <div>Powered by <a href="https://cloud1500.com" target="_blank" rel="nofollow">Cloud1500.com</a></div>
    </div>
  </div>
</footer>
