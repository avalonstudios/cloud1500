@php
$postID = get_the_ID();
$thumb = get_the_post_thumbnail_url( $postID, 'full' );
$thumb = aq_resize( $thumb, 400, 500, true, true, true );
@endphp

<article @php post_class() @endphp>
  @if ( has_post_thumbnail(  ) )
  <div class="post-thumbnail"><a href="{{ get_permalink() }}" rel="bookmark"><img src="{{ $thumb }}" alt="{{ get_the_title() }}"></a></div>
  @else
  <div class="post-thumbnail"><a href="{{ get_permalink() }}" rel="bookmark"><img src="@asset( 'images/cloud1500-logo-only.png' )" alt="{{ get_the_title() }}"></a></div>
  @endif
  <header>
    <h2 class="entry-title"><a href="{{ get_permalink() }}" rel="bookmark">{!! get_the_title() !!}</a></h2>
    @include('partials/entry-meta')
  </header>
  <div class="entry-summary">
    @php the_excerpt() @endphp
  </div>
  @php
  $button[ 'url' ] = get_the_permalink();
  $button[ 'target' ] = '';
  $button[ 'title' ] = get_the_title();
  @endphp
  @include('comps.btns.btn')
</article>
