@php
$socialIcons = App::getOptions(  )[ 'social_icons' ][ 'social_icons' ];
@endphp

<div class="social-menu">
  <ul class="social-icons {{ $place }}">
      @each( 'comps.social-icon-comp', $socialIcons, 'sIcon' )
  </ul>
</div>
