<div class="hamburger"><i class="fas fa-bars"></i></div>
<nav class="navbar fixed-top navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav">
  <div class="main-navigation" id="mainMenuNav">
    @if ( ! is_front_page() )
    {!! wp_nav_menu([
      'theme_location' => 'primary_navigation',
      'container_class' => 'main-menu',
      'menu_class' => 'navbar-nav mr-auto',
      'walker' => new MDBootstrapMenu(  )
      ])
    !!}
    @else
    {!! wp_nav_menu([
      'theme_location' => 'front_navigation',
      'container_class' => 'main-menu',
      'menu_class' => 'navbar-nav mr-auto',
      'walker' => new MDBootstrapMenu(  )
      ])
    !!}
    @endif

    @include ( 'partials.menus.social-menu', [ 'place' => 'social-header' ] )
  </div>
  <div class="nav-underlay-bottom"></div>
  <div class="nav-underlay-left"></div>
</nav>
