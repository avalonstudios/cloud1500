<div class="page-navigation">
  @if ( get_preview_post_link() )
  <div class="prev-link">
    {!! previous_post_link( '&laquo; %link' ) !!}
  </div>
  @endif
  @if ( get_next_post_link() )
  <div class="next-link">
    {!! next_post_link( '%link &raquo;' ) !!}
  </div>
  @endif
</div>
