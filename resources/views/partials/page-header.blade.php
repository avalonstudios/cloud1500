@if ( ! is_front_page() )
  <div class="page-header-wrapper">
    <div class="page-header">
      <h1>{!! App::title() !!}</h1>
    </div>
  </div>
@endif
