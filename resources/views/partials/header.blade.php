<header class="banner">
  <div class="container">
    <div class="site-logo">
      <div class="navbar-brand" href="#">
          {!! App::getCustomLogo() !!}
      </div>
      <a class="brand" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>
    </div>
    @include ( 'partials.menus.main-menu' )
  </div>
</header>
