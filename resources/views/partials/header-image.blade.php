@php
if ( !has_post_thumbnail() ) {
  $hasNoThumb = ' has-no-thumbnail';
} else {
  $hasNoThumb = '';
}
@endphp

@if ( $header_image )
<div class="page-header-image{{ $hasNoThumb }}" style="background-image: url( '{{ $header_image }}' )">
  @include('partials.page-header')
</div>
@endif
