{{--
  Title: Theme Demos Large
  Description: Themes demos large version
  Category: ava_block_category
  Icon: admin-comments
  Keywords: keywords
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields();
$secProp = $flds[ 'sec_prop' ];
$active = $secProp[ 'active' ];

if ( ! $active ) {
  return;
}

$sectionID = $secProp[ 'section_id' ];

$secProps = [
  'backgroundColor' => $secProp[ 'background_color' ],
  'color' => $secProp[ 'text_color' ],
];

$other_classes = '';
$backImg = '';

$sectionTitle = $secProp[ 'section_title' ];

$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'blockID'         => $sectionID,
  'secProps'        => $secProps
];

$demos = $flds[ 'theme_demos' ];
$contactFormID = $flds[ 'contact_form' ];

@endphp

@component( 'comps.blocks', $componentVars )
  <div class="themes-demos">
    @foreach ($demos as $demo)
      @php
      $image = $demo[ 'images' ][ 'url' ];
      $img = aq_resize( $image, 400, null, false );
      $props = $demo[ 'properties' ];
      if ( $props ) {
        $props = explode( "\n", $props );
      }
      @endphp
      <div class="theme-demo">
        <div class="image"><img loading="lazy" src="{{ $img }}" alt=""></div>

        @if ( $props )
          <div class="properties">
            <h3>Features</h3>
            @foreach ($props as $prop)
              <span class="property"><i class="fas fa-check"></i>{{ $prop }}</span>
            @endforeach
          </div>
        @endif
        <div class="btn-wrapper">
          @php
          $button[ 'url' ] = '#contact_form';
          $button[ 'target' ] = '';
          $button[ 'title' ] = 'contact us';
          @endphp
          @include ( 'comps.btns.btn' )
        </div>
      </div>
    @endforeach
  </div>

  @if ( $contactFormID )
    <div id="contact_form" class="contact-form">
      <div class="contact-form-wrapper">
        <h2 class="section-title">Contact Form</h2>
        @php
        echo do_shortcode("[formidable id='{$contactFormID}']");
        @endphp
      </div>
    </div>
  @endif
@endcomponent
