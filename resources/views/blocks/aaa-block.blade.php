{{--
  Title: Block_Template
  Description: Block_Description
  Category: ava_block_category
  Icon: admin-comments
  Keywords: keywords
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$secProp = $flds[ 'sec_prop' ];
$active = $secProp[ 'active' ];

if ( ! $active ) {
  return;
}

$sectionID = $secProp[ 'section_id' ];

$secProps = [
  'backgroundColor' => $secProp[ 'background_color' ],
  'color' => $secProp[ 'text_color' ],
];


$other_classes = '';
$backImg = '';

$sectionTitle = $secProp[ 'section_title' ];

$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'blockID'         => $sectionID,
  'secProps'        => $secProps
];
@endphp

@component( 'comps.blocks', $componentVars )
  <pre>@dump($flds)</pre>
@endcomponent
