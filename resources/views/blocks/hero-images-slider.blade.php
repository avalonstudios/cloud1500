{{--
  Title: Hero Images Slider
  Description:
  Category: ava_block_category
  Icon: admin-comments
  Keywords: keywords
  Mode: edit
  Align: full
  PostTypes: page
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$secProp = $flds[ 'sec_prop' ];
$active = $secProp[ 'active' ];

if ( ! $active ) {
  return;
}

$sectionID = $secProp[ 'section_id' ];

$secProps = [
  'backgroundColor' => $secProp[ 'background_color' ],
  'color' => $secProp[ 'text_color' ],
];

$other_classes = '';
$backImg = '';

$sectionTitle = $secProp[ 'section_title' ];

$slides = $flds[ 'slides' ];

$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'blockID'         => $sectionID,
  'secProps'        => $secProps
];
@endphp

@component( 'comps.blocks', $componentVars )
  <div class="hero-image-slider">
    @foreach ( $slides as $slide )
      @php
      $image = $slide[ 'image' ][ 'url' ];
      $img = aq_resize( $image, 1920, 1080, true, true, true );
      $title = $slide[ 'title' ];
      $text = $slide[ 'text' ];
      $button = $slide[ 'button' ];
      if ( $loop->first ) {
        $wrapClass = ' animated fadeInUp';
      } else {
        $wrapClass = ' animated';
      }
      @endphp
      <div class="hero-slide" style="background-image: url('{{ $img }}');">
        @if ( $title || $text )
        <div class="hero-content animated {{ $loop->first ? 'fadeIn' : '' }}">
          <h2 class="hero-title animated {{ $loop->first ? 'fadeInLeft' : '' }}">{{ $title }}</h2>
          <div class="hero-text animated {{ $loop->first ? 'fadeInRight' : '' }}">{{ $text }}</div>
          @include ( 'comps.btns.btn', [ 'type' => 'white', 'wrapClass' => $wrapClass ] )
        </div>
        @endif
      </div>
    @endforeach
  </div>
  <button id="scroll-hero-image"><a href="#hero-image-end"><i class="fas fa-chevron-down"></i></a></button>
  <div id="hero-image-end"></div>
@endcomponent
