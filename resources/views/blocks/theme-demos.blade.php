{{--
  Title: Theme Demos
  Description: Display image
  Category: ava_block_category
  Icon: admin-comments
  Keywords: keywords
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$secProp = $flds[ 'sec_prop' ];
$active = $secProp[ 'active' ];

if ( ! $active ) {
  return;
}

$demoImages = $flds[ 'theme_demo_images' ];

if ( ! $demoImages ) {
  return;
}

$backImg = $flds[ 'background_image' ];
$backImg = aq_resize( $backImg, 1920, 400, true, true, true );
$desc = wpautop( wp_kses_post( $flds[ 'description' ] ) );

$sectionID = $secProp[ 'section_id' ];

$secProps = [
  'backgroundColor' => $secProp[ 'background_color' ],
  'color' => $secProp[ 'text_color' ],
];

$button = $flds[ 'link' ];

$other_classes = '';
// $backImg = '';

$sectionTitle = $secProp[ 'section_title' ];

$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => '',
  'blockID'         => $sectionID,
  'secProps'        => $secProps
];
@endphp

@component( 'comps.blocks', $componentVars )
  <div class="title-wrapper" data-aos="fade-up" style="background-image: url( '{{ $backImg }}' );">
    <h3 class="title">{{ $sectionTitle }}</h3>
    <div class="underlay"></div>
  </div>
  <div class="text-area">{!! $desc !!}</div>
  <div class="themes-demo-wrapper">
    <div class="themes-demo">
      @foreach ($demoImages as $image)
        @php
        $img = $image[ 'image' ][ 'url' ];
        $img = aq_resize( $img, 9999, 400, false );
        $desc = $image[ 'description' ];
        @endphp
        <div class="theme-demo">
          <img src="{{ $img }}" alt="">
          @if ( $desc )
            <div class="description"><i class="fas fa-quote-left"></i>{{ $desc }}</div>
          @endif
        </div>
      @endforeach
    </div>
  </div>
  <div class="btn-wrapper">
    @include ( 'comps.btns.btn' )
  </div>
@endcomponent
