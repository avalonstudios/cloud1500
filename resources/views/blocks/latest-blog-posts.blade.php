{{--
  Title: Latest Blog Posts
  Description: Displays latest blog posts
  Category: ava_block_category
  Icon: admin-comments
  Keywords: keywords
  Mode: edit
  Align: full
  PostTypes: page
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$secProp = $flds[ 'sec_prop' ];
$active = $secProp[ 'active' ];

if ( ! $active ) {
  return;
}

$sectionID = $secProp[ 'section_id' ];

$secProps = [
  'backgroundColor' => $secProp[ 'background_color' ],
  'color' => $secProp[ 'text_color' ],
];

$other_classes = '';
$backImg = '';

$sectionTitle = $secProp[ 'section_title' ];

$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'blockID'         => $sectionID,
  'secProps'        => $secProps
];

$numOfPosts = $flds[ 'number_of_posts' ];

$args = [
  'posts_per_page' => $numOfPosts
];

$pposts = get_posts( $args );

@endphp

@component( 'comps.blocks', $componentVars )
  <div class="articles-slider">
    @foreach ( $pposts as $ppost )
      @php
      $postID = $ppost->ID;
      $postClasses = get_post_class( '', $postID );
      $postClasses = implode( ' ', $postClasses );
      $thumb = get_the_post_thumbnail_url( $postID, 'full' );
      $thumb = aq_resize( $thumb, 800, 494, true, true, true );
      $title = $ppost->post_title;
      $summary = get_the_excerpt( $postID );
      //$summary = substr( $summary, 0, 300 );
      $link = get_the_permalink( $postID );
      $button[ 'url' ] = $link;
      $button[ 'title' ] = 'read more';
      $button[ 'target' ] = '';
      @endphp
      <article class="{{ $postClasses }} ava-grid">
        @if ( has_post_thumbnail(  ) )
        <div class="post-thumbnail"><img src="{{ $thumb }}" alt="{{ $title }}"></div>
        @else
        <div class="post-thumbnail"><img src="@asset( 'images/cloud1500-logo-only.png' )" alt="{{ $title }}"></div>
        @endif
        <div class="article-content">
          <div class="post-content">
            <a href="{{ $link }}" class="article-link" rel="bookmark">
              <h2 class="title">{{ $title }}</h2>
            </a>
            @include('partials.entry-meta')
            <div class="summary">{!! $summary !!}</div>
          </div>
          @include ( 'comps.btns.btn' )
        </div>
      </article>
    @endforeach
  </div>
@endcomponent
