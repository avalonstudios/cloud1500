{{--
  Title: Squares + List
  Description: Squares with summary, click for more info
  Category: ava_block_category
  Icon: admin-comments
  Keywords: square, summary, info, list
  Mode: edit
  Align: full
  PostTypes: page
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$secProp = $flds[ 'sec_prop' ];
$active = $secProp[ 'active' ];

if ( ! $active ) {
  return;
}

$sectionID = $secProp[ 'section_id' ];

$secProps = [
  'backgroundColor' => $secProp[ 'background_color' ],
  'color' => $secProp[ 'text_color' ],
];

$other_classes = '';
$backImg = '';

$sectionTitle = $secProp[ 'section_title' ];

$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'blockID'         => $sectionID,
  'secProps'        => $secProps
];
@endphp

@component( 'comps.blocks', $componentVars )
  @php
  $squares = $flds[ 'squares' ];
  @endphp
  <div class="squares-wrapper">
    <div class="squares">
      @foreach ( $squares as $square )
        @php
        $contentID       = $square[ 'content_id' ];
        $image           = $square[ 'banner_image' ][ 'url' ];
        $image           = aq_resize( $image, 1920, 400, true, true, true );
        $title           = $square[ 'title' ];
        $summary         = $square[ 'summary' ];
        $summary         = wp_kses_post( $summary );
        $bulletsOrText   = $square[ 'bullet_points_text' ];
        $bulletPoints    = $square[ 'bullet_points' ];
        $text            = $square[ 'text' ];
        $text            = wp_kses_post( $text );
        @endphp
        <div id="{{ $contentID }}" class="square square-{{ $loop->iteration }}">
          <div class="title-wrapper" data-aos="fade-up" style="background-image: url( '{{ $image }}' );">
            <h3 class="title">{{ $title }}</h3>
            <div class="underlay"></div>
          </div>
          <div class="square-contents" data-aos="flip-down">
            <div class="long-summary" data-aos="flip-up">{!! $summary !!}</div>
            @if ( $bulletsOrText == 'bullets' )
              <div class="square-bullet-list">
                @foreach ( $bulletPoints as $bullet )
                  @php
                  $bp = $bullet[ 'bullet_point' ];
                  @endphp
                  @if ( $bp )
                  <div class="square-bullet-point" data-aos="fade-zoom-in" data-aos-delay="{{ 150 * $loop->iteration }}"><i class="fas fa-caret-right"></i>{{ $bp }}</div>
                  @endif
                @endforeach
              </div>
            @elseif ( $bulletsOrText == 'text' )
              <div class="text" data-aos="flip-down">{!! $text !!}</div>
            @endif
          </div><!-- square-contents -->
          {{--<span class="text-grey text-small more-text">More...</span>--}}
        </div><!-- square -->
      @endforeach
    </div>
  </div><!-- squares -->
  {{--<pre>@dump($flds[ 'squares' ])</pre>--}}
@endcomponent
