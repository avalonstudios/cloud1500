@php
if ( ! $button ) {
  return;
}
$url     = $button[ 'url' ];
$target  = $button[ 'target' ];
$title   = $button[ 'title' ];
@endphp

<div class="btn-wrap{{ $wrapClass ?? '' }} wrap-{{ $type ?? 'primary' }}">
  <div class="top-line"></div>
  <a href="{{ $url }}" class="btn btn-{{ $type ?? 'primary' }}{{ $classes ?? '' }}"{{ $target ? 'target="_blank"' : '' }}>{{ $title ?? 'more' }}<span class="cursor"></span></a>
  <div class="bottom-line"></div>
</div>
