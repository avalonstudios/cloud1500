@php
$icon = $sIcon[ 'social_medium' ][ 'value' ];
$title = $sIcon[ 'social_medium' ][ 'label' ];
$link = $sIcon[ 'link' ];
@endphp
<li class="nav-item">
    <a class="nav-link waves-effect" href="{{ $link }}" target="_blank" title="{{ $title }}">
        <i class="fab {{ $icon }}"></i>
    </a>
</li>
