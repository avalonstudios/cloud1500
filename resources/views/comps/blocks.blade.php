<section
  data-{{ $id }}
  id="{{ $blockID }}"
  class="ava-block {{ $classes }}{{ $other_classes }}"
  >

  <div class="ava-block-wrapper {{ $slug }}-wrapper">
    @if ( $title )
      <h2 class="section-title">{{ $title }}</h2>
    @endif
    {{ $slot }}
  </div>
</section>

<style type="text/css">
  [data-{{ $id }} ] {
    color: {{ $secProps[ 'color' ] }};
    background-color: {{ $secProps[ 'backgroundColor' ] }};
  }
  </style>
{{--
--}}
